package org.cainiao.portal.controller.client.knowledge;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.cainiao.portal.dto.response.knowledge.ChartGraph;
import org.cainiao.portal.service.KnowledgePointService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 知识点
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@RestController
@RequiredArgsConstructor
@Tag(name = "KnowledgePoint", description = "知识点")
public class KnowledgePointController {

    private final KnowledgePointService knowledgePointService;

    @GetMapping("knowledge-area/{id}/published/knowledge-point/chart")
    @Operation(summary = "查询知识领域下已发布的知识点图表")
    public ChartGraph publishedKnowledgePointChart(@Parameter(description = "知识领域 ID") @PathVariable Long id) {
        return knowledgePointService.publishedKnowledgePointChart(id);
    }
}
