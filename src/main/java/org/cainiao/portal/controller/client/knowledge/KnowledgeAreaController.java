package org.cainiao.portal.controller.client.knowledge;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.cainiao.portal.dto.response.knowledge.ChartTreeNode;
import org.cainiao.portal.entity.knowledge.KnowledgeArea;
import org.cainiao.portal.service.KnowledgeAreaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.cainiao.portal.dao.DaoUtil.DEFAULT_PAGE;
import static org.cainiao.portal.dao.DaoUtil.DEFAULT_PAGE_SIZE;

/**
 * 知识领域
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@RestController
@RequiredArgsConstructor
@Tag(name = "KnowledgeArea", description = "知识领域")
public class KnowledgeAreaController {

    private final KnowledgeAreaService knowledgeAreaService;

    @GetMapping("knowledge-area/page")
    @Operation(summary = "分页模糊查询知识领域列表")
    public IPage<KnowledgeArea> knowledgeAreaPage(
        @Parameter(description = "当前页") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int current,
        @Parameter(description = "每页显示记录数") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size,
        @Parameter(description = "搜索关键词") @RequestParam(required = false) String key) {

        return knowledgeAreaService.knowledgeAreaPage(current, size, key);
    }

    @GetMapping("published/knowledge-area/chart")
    @Operation(summary = "查询知识领域图表")
    public List<ChartTreeNode> publishedKnowledgeAreaChart() {
        return knowledgeAreaService.publishedKnowledgeAreaChart();
    }
}
