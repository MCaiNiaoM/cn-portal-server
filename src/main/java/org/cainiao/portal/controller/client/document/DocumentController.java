package org.cainiao.portal.controller.client.document;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.cainiao.portal.dto.response.TreeNode;
import org.cainiao.portal.dto.response.document.CnBlock;
import org.cainiao.portal.dto.response.document.LarkFolderDocument;
import org.cainiao.portal.dto.response.document.StyleMask;
import org.cainiao.portal.service.DocumentService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.cainiao.portal.dao.DaoUtil.DEFAULT_PAGE;
import static org.cainiao.portal.dao.DaoUtil.DEFAULT_PAGE_SIZE;

/**
 * 菜鸟笔记是一棵树，每个节点都是菜鸟笔记的数据库记录，节点有多重类型：<br />
 * <ol>
 *     <li>飞书文档，文档中的所有数据来自飞书，菜鸟笔记不入库</li>
 *     <li>飞书文件夹，文件夹中的所有数据来自飞书，菜鸟笔记不入库</li>
 * </ol>
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@RestController
@RequiredArgsConstructor
@Tag(name = "Document", description = "笔记操作")
public class DocumentController {

    private final DocumentService documentService;

    @GetMapping("knowledge-point/{id}/published/documents")
    @Operation(summary = "查询知识点下已发布的文档")
    public List<TreeNode> publishedDocuments(@Parameter(description = "知识点 ID") @PathVariable Long id) {
        return documentService.publishedDocuments(id);
    }

    @PostMapping("/{token}/lark-notes")
    @Operation(summary = "查询飞书子节点笔记")
    public IPage<TreeNode> getLarkChildren(
        @Parameter(description = "飞书文件夹 Token") @PathVariable("token") String token,
        @Parameter(description = "当前页") @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int current,
        @Parameter(description = "每页显示记录数") @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size) {

        return documentService.getLarkChildren(token, current, size);
    }

    @PostMapping("/{documentId}/lark-blocks")
    @Operation(summary = "查询飞书文档的所有块")
    public List<CnBlock> getLarkBlocks(
        @Parameter(description = "文档的唯一标识") @PathVariable("documentId") String documentId) {

        return documentService.getLarkBlocks(documentId);
    }

    @PostMapping("lark-folder/{folderToken}")
    @Operation(summary = "创建顶层飞书文件夹同步文档")
    public LarkFolderDocument createRootLarkFolderDocument(
        @AuthenticationPrincipal AuthenticatedPrincipal principal,
        @Parameter(description = "飞书文件夹 Token", required = true) @PathVariable("folderToken") String folderToken) {

        return documentService.createLarkFolderDocument(principal.getName(), null, folderToken);
    }

    @PostMapping("document/{id}/lark-folder/{folderToken}")
    @Operation(summary = "创建飞书文件夹同步文档")
    public LarkFolderDocument createLarkFolderDocument(
        @AuthenticationPrincipal AuthenticatedPrincipal principal,
        @Parameter(description = "文档 ID，创建的飞书文件夹同步文档位于这个文档之下") @PathVariable("id") long id,
        @Parameter(description = "飞书文件夹 Token", required = true) @PathVariable("folderToken") String folderToken) {

        return documentService.createLarkFolderDocument(principal.getName(), id, folderToken);
    }

    @GetMapping("style-masks")
    @Operation(summary = "密林编辑器的样式掩码")
    public StyleMask styleMask() {
        return StyleMask.getInstance();
    }

    @GetMapping("/board/whiteboards/{whiteboardId}/download_as_image")
    @Operation(summary = "获取画板缩略图片")
    public ResponseEntity<Resource> whiteboardImage(
        @Parameter(description = "画板唯一标识") @PathVariable("whiteboardId") String whiteboardId) {

        return documentService.whiteboardImage(whiteboardId);
    }
}
