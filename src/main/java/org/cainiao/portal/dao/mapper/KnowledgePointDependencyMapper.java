package org.cainiao.portal.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.cainiao.portal.entity.knowledge.KnowledgePointDependency;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface KnowledgePointDependencyMapper extends BaseMapper<KnowledgePointDependency> {
}
