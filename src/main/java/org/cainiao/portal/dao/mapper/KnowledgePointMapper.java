package org.cainiao.portal.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.cainiao.portal.entity.knowledge.KnowledgePoint;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface KnowledgePointMapper extends BaseMapper<KnowledgePoint> {
}
