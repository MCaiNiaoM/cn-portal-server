package org.cainiao.portal.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.cainiao.portal.entity.document.Document;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface DocumentMapper extends BaseMapper<Document> {

    List<Document> findDocumentByKnowledgePointIdAndPublished(@Param("knowledgePointId") long knowledgePointId,
                                                              @Param("published") boolean published);
}
