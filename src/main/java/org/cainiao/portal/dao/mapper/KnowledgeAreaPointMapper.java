package org.cainiao.portal.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.cainiao.portal.entity.knowledge.KnowledgeAreaPoint;
import org.cainiao.portal.entity.knowledge.KnowledgePoint;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface KnowledgeAreaPointMapper extends BaseMapper<KnowledgeAreaPoint> {

    List<KnowledgePoint> findKnowledgePointByAreaIdAndPublished(@Param("knowledgeAreaId") Long knowledgeAreaId,
                                                                @Param("published") boolean published);
}
