package org.cainiao.portal.dao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.cainiao.portal.dao.mapper.KnowledgePointMapper;
import org.cainiao.portal.entity.knowledge.KnowledgePoint;
import org.springframework.stereotype.Service;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Service
public class KnowledgePointMapperService
    extends ServiceImpl<KnowledgePointMapper, KnowledgePoint> implements IService<KnowledgePoint> {

}
