package org.cainiao.portal.dao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.cainiao.portal.dao.mapper.KnowledgeAreaPointMapper;
import org.cainiao.portal.entity.knowledge.KnowledgeAreaPoint;
import org.cainiao.portal.entity.knowledge.KnowledgePoint;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Service
public class KnowledgeAreaPointMapperService
    extends ServiceImpl<KnowledgeAreaPointMapper, KnowledgeAreaPoint> implements IService<KnowledgeAreaPoint> {

    public List<KnowledgePoint> findKnowledgePointByAreaIdAndPublished(Long knowledgeAreaId, boolean published) {
        return getBaseMapper().findKnowledgePointByAreaIdAndPublished(knowledgeAreaId, published);
    }
}
