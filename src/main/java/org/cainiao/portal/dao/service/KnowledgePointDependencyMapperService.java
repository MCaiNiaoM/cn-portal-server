package org.cainiao.portal.dao.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.cainiao.portal.dao.mapper.KnowledgePointDependencyMapper;
import org.cainiao.portal.entity.knowledge.KnowledgePointDependency;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Service
public class KnowledgePointDependencyMapperService
    extends ServiceImpl<KnowledgePointDependencyMapper, KnowledgePointDependency>
    implements IService<KnowledgePointDependency> {

    public List<KnowledgePointDependency> findKnowledgePointDependencies(Set<Long> knowledgePointIds) {
        return lambdaQuery().in(KnowledgePointDependency::getKnowledgePointId, knowledgePointIds)
            .in(KnowledgePointDependency::getDependencyKnowledgePointId, knowledgePointIds).list();
    }
}
