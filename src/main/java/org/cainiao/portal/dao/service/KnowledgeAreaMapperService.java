package org.cainiao.portal.dao.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.cainiao.portal.dao.mapper.KnowledgeAreaMapper;
import org.cainiao.portal.entity.knowledge.KnowledgeArea;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Service
public class KnowledgeAreaMapperService
    extends ServiceImpl<KnowledgeAreaMapper, KnowledgeArea> implements IService<KnowledgeArea> {

    public IPage<KnowledgeArea> searchPage(int current, int size, String key) {
        IPage<KnowledgeArea> page = new Page<>(current, size);
        return StringUtils.hasText(key)
            ? page(page, lambdaQuery()
            .like(KnowledgeArea::getName, key).or())
            : page(page);
    }

    public List<KnowledgeArea> findByPublished(boolean published) {
        return lambdaQuery().eq(KnowledgeArea::isPublished, published)
            .orderByAsc(KnowledgeArea::getParentId, KnowledgeArea::getOrder).list();
    }
}
