package org.cainiao.portal.util.document;

import lombok.experimental.UtilityClass;
import org.cainiao.api.lark.dto.response.docs.docs.apireference.document.LarkBlock;
import org.cainiao.api.lark.dto.response.docs.docs.apireference.document.LarkCallout;
import org.cainiao.api.lark.dto.response.docs.docs.apireference.document.text.LarkTextStyle;
import org.cainiao.portal.dto.response.document.CnBlock;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@UtilityClass
public class BlockStyleMaskSupport {

    private static final byte STYLE_OFFSET_FOLD = 1;
    private static final byte STYLE_OFFSET_FOLDED = STYLE_OFFSET_FOLD + 1;
    private static final byte STYLE_OFFSET_WRAP = STYLE_OFFSET_FOLDED + 1;
    private static final byte STYLE_OFFSET_LANGUAGE = STYLE_OFFSET_WRAP + 1;
    private static final byte STYLE_OFFSET_WIDTH_RATIO = STYLE_OFFSET_LANGUAGE + 7;
    private static final byte STYLE_OFFSET_BACKGROUND_COLOR = STYLE_OFFSET_WIDTH_RATIO + 7;
    private static final byte STYLE_OFFSET_BORDER_COLOR = STYLE_OFFSET_BACKGROUND_COLOR + 5;
    // 32, 31, [30, 29], 28 ...
    private static final byte STYLE_OFFSET_ALIGN = STYLE_OFFSET_BORDER_COLOR + 5;

    /**
     * 掩码：代办是否完成（第 1 位）
     */
    public static final int BLOCK_STYLE_MASK_DONE = 1;
    /**
     * 掩码：是否可折叠（第 2 位）
     */
    public static final int BLOCK_STYLE_MASK_FOLD = 1 << STYLE_OFFSET_FOLD;
    /**
     * 掩码：是否折叠（第 3 位）
     */
    public static final int BLOCK_STYLE_MASK_FOLDED = 1 << STYLE_OFFSET_FOLDED;
    /**
     * 掩码：代码块是否自动换行（第 4 位）
     */
    public static final int BLOCK_STYLE_MASK_WRAP = 1 << STYLE_OFFSET_WRAP;
    /**
     * 掩码：代码块的语言（第 5 ~ 11 位）
     */
    public static final int BLOCK_STYLE_MASK_LANGUAGE = ((1 << 7) - 1) << STYLE_OFFSET_LANGUAGE;
    /**
     * 掩码：栅格的宽度比例（第 12 ~ 18 位）
     */
    public static final int BLOCK_STYLE_MASK_WIDTH_RATIO = ((1 << 7) - 1) << STYLE_OFFSET_WIDTH_RATIO;
    /**
     * 掩码：背景颜色（第 19 ~ 23 位）
     */
    public static final int BLOCK_STYLE_MASK_BACKGROUND_COLOR = ((1 << 5) - 1) << STYLE_OFFSET_BACKGROUND_COLOR;
    /**
     * 掩码：边线颜色（第 24 ~ 28 位）
     */
    public static final int BLOCK_STYLE_MASK_BORDER_COLOR = ((1 << 5) - 1) << STYLE_OFFSET_BORDER_COLOR;
    /**
     * 掩码：align，对齐方式：0 靠左；1 居中；2 靠右（第 29 ~ 30 位）
     */
    public static final int BLOCK_STYLE_MASK_ALIGN = ((1 << 2) - 1) << STYLE_OFFSET_ALIGN;

    // 将 “是否完成” 标志位置 1
    public static void setDoneToStyle(CnBlock cnBlock) {
        cnBlock.setStyle(cnBlock.getStyle() | BLOCK_STYLE_MASK_DONE);
    }

    // 将 “代码是否自动换行” 标志位置 1
    public static void setWrapToStyle(CnBlock cnBlock) {
        cnBlock.setStyle(cnBlock.getStyle() | BLOCK_STYLE_MASK_WRAP);
    }

    // 将 “是否可折叠” 标志位置 1
    public static void setFoldToStyle(CnBlock cnBlock) {
        cnBlock.setStyle(cnBlock.getStyle() | BLOCK_STYLE_MASK_FOLD);
    }

    // 将 “是否折叠” 标志位置 1
    public static void setFoldedToStyle(CnBlock cnBlock) {
        cnBlock.setStyle(cnBlock.getStyle() | BLOCK_STYLE_MASK_FOLDED);
    }

    public static void setBackgroundColorAndBorderColorToStyle(CnBlock cnBlock, LarkCallout larkCallout) {
        cnBlock.setStyle(cnBlock.getStyle()
            | (larkCallout.getBackgroundColor() << STYLE_OFFSET_BACKGROUND_COLOR)
            | (larkCallout.getBorderColor() << STYLE_OFFSET_BORDER_COLOR));
    }

    public static void setWidthRatioToStyle(CnBlock cnBlock, LarkBlock larkBlock) {
        cnBlock.setStyle(cnBlock.getStyle() | (larkBlock.getGridColumn().getWidthRatio() << STYLE_OFFSET_WIDTH_RATIO));
    }

    public static void setLanguageToStyle(CnBlock cnBlock, LarkTextStyle larkTextStyle) {
        cnBlock.setStyle(cnBlock.getStyle() | (larkTextStyle.getLanguage() << STYLE_OFFSET_LANGUAGE));
    }

    public static void setAlignToStyle(CnBlock cnBlock, LarkTextStyle larkTextStyle) {
        Integer larkAlign = larkTextStyle.getAlign();
        if (larkAlign == null) {
            return;
        }
        cnBlock.setStyle(cnBlock.getStyle() | ((larkAlign - 1) << STYLE_OFFSET_ALIGN));
    }
}
