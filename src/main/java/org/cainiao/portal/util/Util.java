package org.cainiao.portal.util;

import lombok.experimental.UtilityClass;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@UtilityClass
public class Util {

    /**
     * 获取第一个下划线后的子字符串<br />
     * 如果原字符串中没有下划线或下划线后没有字符，则返回原字符串
     *
     * @param origin 原字符串
     * @return 第一个下划线后的子字符串
     */
    public static String getSubstringAfterUnderscore(String origin) {
        int underscoreIndex = origin.indexOf("_");
        if (underscoreIndex == -1 || underscoreIndex == origin.length() - 1) {
            return origin;
        } else {
            return origin.substring(underscoreIndex + 1);
        }
    }
}
