package org.cainiao.portal.util;

import lombok.experimental.UtilityClass;
import org.cainiao.common.util.DDLGenerator;
import org.cainiao.oauth2.client.core.entity.CnClientRegistration;
import org.cainiao.portal.entity.document.Document;
import org.cainiao.portal.entity.knowledge.KnowledgeArea;

import java.io.IOException;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@UtilityClass
public class GenerateUtil {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        DDLGenerator ddlGenerator = DDLGenerator
            .NEW("src/main/resources/sql/ddl.sql", KnowledgeArea.class, CnClientRegistration.class, Document.class);
        ddlGenerator.generateDDL();
    }
}
