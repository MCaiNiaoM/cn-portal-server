package org.cainiao.portal.entity.document;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.cainiao.common.constant.ICodeBook;
import org.cainiao.common.dao.ColumnDefine;
import org.cainiao.common.dao.IdBaseEntity;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_document")
@Schema(name = "Document", description = "文档")
public class Document extends IdBaseEntity {

    @Serial
    private static final long serialVersionUID = -5186284820110924015L;

    @TableField(value = "d_owner_open_id", insertStrategy = FieldStrategy.NOT_EMPTY)
    @Schema(description = "所有者 openId")
    private String ownerOpenId;

    @TableField(value = "d_parent_id")
    @Schema(description = "父文档 ID")
    private Long parentId;

    @TableField(value = "d_type", insertStrategy = FieldStrategy.NOT_NULL)
    @Schema(description = "文档类型")
    private DocumentTypeEnum type;

    /**
     * 外部 ID<br />
     * 当数据需要从外部获取时，记录获取外部数据需要的 ID<br />
     * 例如飞书文档或飞书文件夹的 Token
     */
    @TableField(value = "d_external_id")
    @Schema(description = "外部 ID")
    private String externalId;

    @TableField(value = "d_name")
    @Schema(description = "文档名称")
    private String name;

    @TableField(value = "d_order", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "顺序")
    private long order;

    @TableField(value = "d_published", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "是否公开")
    private boolean published;

    @RequiredArgsConstructor
    @Getter
    public enum DocumentTypeEnum implements ICodeBook {
        NORMAL("普通笔记", "普通笔记"),
        LARK_DOCUMENT("飞书文档笔记", "一种笔记类型，通过同步飞书文档获取数据"),
        LARK_FOLDER("飞书文件夹笔记", "一种笔记类型，通过同步飞书文件夹获取数据");

        private final String code;
        private final String description;
    }
}
