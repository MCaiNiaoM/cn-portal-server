package org.cainiao.portal.entity.knowledge;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.cainiao.common.dao.IdBaseEntity;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_knowledge_point_dependency")
@Schema(name = "KnowledgePointDependency", description = "知识点依赖")
public class KnowledgePointDependency extends IdBaseEntity {

    @Serial
    private static final long serialVersionUID = -8609656181962772660L;

    @TableField(value = "kpd_knowledge_point_id", insertStrategy = FieldStrategy.NOT_NULL)
    @Schema(description = "知识点 ID")
    private Long knowledgePointId;

    @TableField(value = "kpd_dependency_knowledge_point_id", insertStrategy = FieldStrategy.NOT_NULL)
    @Schema(description = "依赖的知识点 ID")
    private Long dependencyKnowledgePointId;
}
