package org.cainiao.portal.entity.knowledge;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.cainiao.common.dao.IdBaseEntity;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_knowledge_area_point")
@Schema(name = "KnowledgeAreaPoint", description = "知识领域下有哪些知识点")
public class KnowledgeAreaPoint extends IdBaseEntity {

    @Serial
    private static final long serialVersionUID = 4916824417526995913L;

    @TableField(value = "kap_knowledge_area_id", insertStrategy = FieldStrategy.NOT_NULL)
    @Schema(description = "知领域 ID")
    private Long knowledgeAreaId;

    @TableField(value = "kap_knowledge_point_id", insertStrategy = FieldStrategy.NOT_NULL)
    @Schema(description = "知识点 ID")
    private Long knowledgePointId;
}
