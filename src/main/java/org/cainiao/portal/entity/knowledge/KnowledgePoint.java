package org.cainiao.portal.entity.knowledge;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.cainiao.common.dao.ColumnDefine;
import org.cainiao.common.dao.IdBaseEntity;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_knowledge_point")
@Schema(name = "KnowledgePoint", description = "知识点")
public class KnowledgePoint extends IdBaseEntity {

    @Serial
    private static final long serialVersionUID = 5437589068920235036L;

    @TableField(value = "kp_owner_open_id", insertStrategy = FieldStrategy.NOT_EMPTY)
    @Schema(description = "所有者 openId")
    private String ownerOpenId;

    @TableField(value = "kp_name", insertStrategy = FieldStrategy.NOT_EMPTY)
    @Schema(description = "名称")
    private String name;

    @TableField(value = "kp_order", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "顺序")
    private long order;

    @TableField(value = "kp_published", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "是否公开")
    private boolean published;
}
