package org.cainiao.portal.entity.knowledge;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.cainiao.common.dao.ColumnDefine;
import org.cainiao.common.dao.IdBaseEntity;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_knowledge_area")
@Schema(name = "KnowledgeArea", description = "知识领域")
public class KnowledgeArea extends IdBaseEntity {

    @Serial
    private static final long serialVersionUID = 3417783681948242573L;
    
    @TableField(value = "ka_owner_open_id", insertStrategy = FieldStrategy.NOT_EMPTY)
    @Schema(description = "所有者 openId")
    private String ownerOpenId;

    @TableField(value = "ka_parent_id")
    @Schema(description = "所属领域")
    private Long parentId;

    @TableField(value = "ka_name", insertStrategy = FieldStrategy.NOT_EMPTY)
    @Schema(description = "名称")
    private String name;

    @TableField(value = "ka_order", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "顺序")
    private long order;

    @TableField(value = "ka_published", insertStrategy = FieldStrategy.NOT_NULL)
    @ColumnDefine(hasDefault = true)
    @Schema(description = "是否公开")
    private boolean published;
}
