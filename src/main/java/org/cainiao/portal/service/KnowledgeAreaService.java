package org.cainiao.portal.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.cainiao.portal.dto.response.knowledge.ChartTreeNode;
import org.cainiao.portal.entity.knowledge.KnowledgeArea;

import java.util.List;

public interface KnowledgeAreaService {

    IPage<KnowledgeArea> knowledgeAreaPage(int current, int size, String key);

    List<ChartTreeNode> publishedKnowledgeAreaChart();
}
