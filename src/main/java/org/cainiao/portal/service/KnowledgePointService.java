package org.cainiao.portal.service;

import org.cainiao.portal.dto.response.knowledge.ChartGraph;

public interface KnowledgePointService {

    ChartGraph publishedKnowledgePointChart(Long knowledgeAreaId);
}
