package org.cainiao.portal.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.cainiao.portal.dto.response.TreeNode;
import org.cainiao.portal.dto.response.document.CnBlock;
import org.cainiao.portal.dto.response.document.LarkFolderDocument;
import org.cainiao.portal.entity.document.Document;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
public interface DocumentService {

    List<TreeNode> publishedDocuments(Long knowledgePointId);

    LarkFolderDocument createLarkFolderDocument(String systemUserId, Long parentId, String folderToken);

    List<Document> getChildren(Long documentId, String systemUserId);

    IPage<TreeNode> getLarkChildren(String token, int current, int size);

    List<CnBlock> getLarkBlocks(String documentId);

    ResponseEntity<Resource> whiteboardImage(String whiteboardId);
}
