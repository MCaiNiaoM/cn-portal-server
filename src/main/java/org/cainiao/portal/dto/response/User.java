package org.cainiao.portal.dto.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Data
@Builder
@Accessors(chain = true)
@Schema(name = "User", description = "登录用户")
public class User {

    @Schema(description = "用户名", requiredMode = Schema.RequiredMode.REQUIRED)
    private String userName;

    public static User anonymousUser() {
        return null;
    }
}
