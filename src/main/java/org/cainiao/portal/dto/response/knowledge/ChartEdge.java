package org.cainiao.portal.dto.response.knowledge;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@Schema(name = "ChartEdge", description = "图表数据-边")
public class ChartEdge {

    @Schema(description = "起点", requiredMode = Schema.RequiredMode.REQUIRED)
    private long from;

    @Schema(description = "终点", requiredMode = Schema.RequiredMode.REQUIRED)
    private long to;
}
