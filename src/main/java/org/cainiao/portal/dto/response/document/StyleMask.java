package org.cainiao.portal.dto.response.document;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

import static org.cainiao.portal.util.document.BlockStyleMaskSupport.*;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Data
public class StyleMask implements Serializable {

    @Serial
    private static final long serialVersionUID = 1173049401106795136L;

    private volatile static StyleMask styleMask;

    private int blockStyleMaskDone = BLOCK_STYLE_MASK_DONE;
    private int blockStyleMaskFold = BLOCK_STYLE_MASK_FOLD;
    private int blockStyleMaskFolded = BLOCK_STYLE_MASK_FOLDED;
    private int blockStyleMaskWrap = BLOCK_STYLE_MASK_WRAP;
    private int blockStyleMaskLanguage = BLOCK_STYLE_MASK_LANGUAGE;
    private int blockStyleMaskWidthRatio = BLOCK_STYLE_MASK_WIDTH_RATIO;
    private int blockStyleMaskBackgroundColor = BLOCK_STYLE_MASK_BACKGROUND_COLOR;
    private int blockStyleMaskBorderColor = BLOCK_STYLE_MASK_BORDER_COLOR;
    private int blockStyleMaskAlign = BLOCK_STYLE_MASK_ALIGN;

    private int elementStyleMaskBold = CnElement.STYLE_MASK_BOLD;
    private int elementStyleMaskItalic = CnElement.STYLE_MASK_ITALIC;
    private int elementStyleMaskStrikethrough = CnElement.STYLE_MASK_STRIKETHROUGH;
    private int elementStyleMaskUnderline = CnElement.STYLE_MASK_UNDERLINE;
    private int elementStyleMaskInlineCode = CnElement.STYLE_MASK_INLINE_CODE;

    private StyleMask() {
    }

    public static StyleMask getInstance() {
        if (styleMask != null) {
            return styleMask;
        }
        synchronized (StyleMask.class) {
            if (styleMask == null) {
                styleMask = new StyleMask();
            }
            return styleMask;
        }
    }
}
