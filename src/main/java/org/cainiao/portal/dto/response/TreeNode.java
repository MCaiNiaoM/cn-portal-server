package org.cainiao.portal.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

import static org.cainiao.portal.entity.document.Document.DocumentTypeEnum;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@Schema(name = "TreeNode", description = "树节点")
public class TreeNode {

    @Schema(description = "key", requiredMode = Schema.RequiredMode.REQUIRED)
    private String key;

    @Schema(description = "标题", requiredMode = Schema.RequiredMode.REQUIRED)
    private String title;

    @Schema(description = "文档类型", requiredMode = Schema.RequiredMode.REQUIRED)
    private DocumentTypeEnum type;

    @Schema(description = "外部 ID", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private String externalId;

    /**
     * 如果不加 @JsonProperty("isLeaf")，则在 rest controller 的响应 json 中，布尔值字段会被特殊处理成 leaf
     */
    @JsonProperty("isLeaf")
    @Schema(description = "是否为叶子节点", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private boolean isLeaf;

    @Schema(description = "子节点", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private List<TreeNode> children;
}
