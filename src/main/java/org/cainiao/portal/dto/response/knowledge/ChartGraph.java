package org.cainiao.portal.dto.response.knowledge;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Data
@Builder
@Accessors(chain = true)
@Schema(name = "ChartGraph", description = "图表-图")
public class ChartGraph {

    @Schema(description = "图包含的节点", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private List<ChartNode> nodes;

    @Schema(description = "图包含的边", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private List<ChartEdge> edges;
}
