package org.cainiao.portal.dto.response.knowledge;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
@Schema(name = "ChartNode", description = "图表数据-节点")
public class ChartNode {

    @Schema(description = "ID", requiredMode = Schema.RequiredMode.REQUIRED)
    private long id;

    @Schema(description = "名称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String name;

    @Schema(description = "值", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private Integer value;
}
