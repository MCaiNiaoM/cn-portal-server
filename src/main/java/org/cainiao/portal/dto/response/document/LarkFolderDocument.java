package org.cainiao.portal.dto.response.document;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.cainiao.api.lark.dto.response.docs.space.folder.LarkFilePage;
import org.cainiao.portal.entity.document.Document;

import java.io.Serial;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Schema(name = "LarkFolderDocument", description = "飞书文件夹类型的文档")
public class LarkFolderDocument extends Document {

    @Serial
    private static final long serialVersionUID = -9090803763651185697L;

    LarkFilePage files;

    public static LarkFolderDocument fromDocument(Document document, LarkFilePage larkPage) {
        return LarkFolderDocument.builder()
            .parentId(document.getParentId())
            .id(document.getId())
            .type(document.getType())
            .externalId(document.getExternalId())
            .files(larkPage)
            .build();
    }
}
