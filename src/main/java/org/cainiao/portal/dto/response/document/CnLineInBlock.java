package org.cainiao.portal.dto.response.document;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Builder
@Data
public class CnLineInBlock implements Serializable {

    @Serial
    private static final long serialVersionUID = -3775053797496345018L;

    /**
     * 当前行中的文本内容，因为文本的不同部分可能有不同的样式，因此将一个文本拆分为了元素列表
     */
    @Schema(description = "文本内容，因为文本的不同部分可能有不同的样式，因此将一个文本拆分为了元素列表", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private List<CnElement> elements;
}
