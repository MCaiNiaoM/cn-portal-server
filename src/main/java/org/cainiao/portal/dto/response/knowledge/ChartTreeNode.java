package org.cainiao.portal.dto.response.knowledge;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * <br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Schema(name = "ChartTreeNode", description = "图表数据-树节点")
public class ChartTreeNode extends ChartNode {

    @Schema(description = "子节点", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    private List<ChartTreeNode> children;
}
