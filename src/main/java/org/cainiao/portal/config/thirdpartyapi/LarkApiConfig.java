package org.cainiao.portal.config.thirdpartyapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cainiao.api.lark.dto.response.LarkDataResponse;
import org.cainiao.api.lark.dto.response.authenticateandauthorize.getaccesstokens.AppAccessTokenResponse;
import org.cainiao.api.lark.dto.response.authenticateandauthorize.getaccesstokens.LarkTokenInfo;
import org.cainiao.api.lark.dto.response.docs.docs.apireference.document.LarkBlockPage;
import org.cainiao.api.lark.dto.response.docs.space.folder.LarkFilePage;
import org.cainiao.api.lark.dto.response.docs.space.folder.LarkFolderMeta;
import org.cainiao.api.lark.impl.imperative.WebClientLarkApiFactory;
import org.cainiao.api.lark.impl.util.converter.ResponseConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

/**
 * TODO 将 api 包独立为单独的项目后，进行 @AutoConfiguration 自动配置<br />
 * <p>
 * Author: Cai Niao(wdhlzd@163.com)<br />
 */
@Configuration
public class LarkApiConfig {

    @Bean
    WebClientLarkApiFactory webClientLarkApiFactory(WebClient webClient) {
        ObjectMapper objectMapper = new ObjectMapper();
        return WebClientLarkApiFactory.builder()
            .webClient(webClient)
            .baseUrl("https://auth.milin.website/tenant/lark")
            .responseConverter(new ResponseConverter() {
                @Override
                public AppAccessTokenResponse convertToAppAccessTokenResponse(Map<String, Object> responseMap) {
                    return objectMapper.convertValue(responseMap, AppAccessTokenResponse.class);
                }

                @Override
                public LarkDataResponse<LarkTokenInfo> convertToLarkTokenInfoResponse(Map<String, Object> responseMap) {
                    try {
                        return objectMapper.readValue(objectMapper.writeValueAsString(responseMap), new TypeReference<>() {
                        });
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                }

                @Override
                public LarkDataResponse<LarkFolderMeta> convertToLarkFolderMetaResponse(Map<String, Object> responseMap) {
                    try {
                        return objectMapper.readValue(objectMapper.writeValueAsString(responseMap), new TypeReference<>() {
                        });
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                }

                @Override
                public LarkDataResponse<LarkFilePage> convertToLarkFilePageResponse(Map<String, Object> responseMap) {
                    try {
                        return objectMapper.readValue(objectMapper.writeValueAsString(responseMap), new TypeReference<>() {
                        });
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                }

                @Override
                public LarkDataResponse<LarkBlockPage> convertToLarkBlockPageResponse(Map<String, Object> responseMap) {
                    try {
                        return objectMapper.readValue(objectMapper.writeValueAsString(responseMap), new TypeReference<>() {
                        });
                    } catch (JsonProcessingException e) {
                        return null;
                    }
                }
            })
            .build();
    }
}
