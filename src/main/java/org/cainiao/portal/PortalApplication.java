package org.cainiao.portal;

import org.cainiao.oauth2.client.core.dao.mapper.CnClientRegistrationMapper;
import org.cainiao.portal.dao.mapper.KnowledgeAreaMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackageClasses = {KnowledgeAreaMapper.class, CnClientRegistrationMapper.class})
public class PortalApplication {

    public static void main(String[] args) {
        new SpringApplication(PortalApplication.class).run(args);
    }
}
