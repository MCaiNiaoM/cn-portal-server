-- public.t_knowledge_area definition

CREATE TABLE IF NOT EXISTS public.t_knowledge_area (
	ka_owner_open_id varchar NOT NULL, -- 所有者 openId
	ka_parent_id int8 NULL, -- 所属领域
	ka_name varchar NOT NULL, -- 名称
	ka_order int8 DEFAULT 0 NOT NULL, -- 顺序
	ka_published boolean DEFAULT false NOT NULL, -- 是否公开
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_knowledge_area_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_knowledge_area.ka_owner_open_id IS '所有者 openId';
COMMENT ON COLUMN public.t_knowledge_area.ka_parent_id IS '所属领域';
COMMENT ON COLUMN public.t_knowledge_area.ka_name IS '名称';
COMMENT ON COLUMN public.t_knowledge_area.ka_order IS '顺序';
COMMENT ON COLUMN public.t_knowledge_area.ka_published IS '是否公开';
COMMENT ON COLUMN public.t_knowledge_area.created_by IS '创建人';
COMMENT ON COLUMN public.t_knowledge_area.created_at IS '创建时间';
COMMENT ON COLUMN public.t_knowledge_area.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_knowledge_area.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_knowledge_area.id IS '主键 ID';

-- public.t_knowledge_area_point definition

CREATE TABLE IF NOT EXISTS public.t_knowledge_area_point (
	kap_knowledge_area_id int8 NOT NULL, -- 知领域 ID
	kap_knowledge_point_id int8 NOT NULL, -- 知识点 ID
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_knowledge_area_point_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_knowledge_area_point.kap_knowledge_area_id IS '知领域 ID';
COMMENT ON COLUMN public.t_knowledge_area_point.kap_knowledge_point_id IS '知识点 ID';
COMMENT ON COLUMN public.t_knowledge_area_point.created_by IS '创建人';
COMMENT ON COLUMN public.t_knowledge_area_point.created_at IS '创建时间';
COMMENT ON COLUMN public.t_knowledge_area_point.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_knowledge_area_point.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_knowledge_area_point.id IS '主键 ID';

-- public.t_knowledge_point definition

CREATE TABLE IF NOT EXISTS public.t_knowledge_point (
	kp_owner_open_id varchar NOT NULL, -- 所有者 openId
	kp_name varchar NOT NULL, -- 名称
	kp_appearance varchar NOT NULL, -- 外观属性，如：颜色、图片等
	kp_published boolean DEFAULT false NOT NULL, -- 是否公开
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_knowledge_point_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_knowledge_point.kp_owner_open_id IS '所有者 openId';
COMMENT ON COLUMN public.t_knowledge_point.kp_name IS '名称';
COMMENT ON COLUMN public.t_knowledge_point.kp_appearance IS '外观属性，如：颜色、图片等';
COMMENT ON COLUMN public.t_knowledge_point.kp_published IS '是否公开';
COMMENT ON COLUMN public.t_knowledge_point.created_by IS '创建人';
COMMENT ON COLUMN public.t_knowledge_point.created_at IS '创建时间';
COMMENT ON COLUMN public.t_knowledge_point.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_knowledge_point.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_knowledge_point.id IS '主键 ID';

-- public.t_knowledge_point_dependency definition

CREATE TABLE IF NOT EXISTS public.t_knowledge_point_dependency (
	kpd_knowledge_point_id int8 NOT NULL, -- 知识点 ID
	kpd_dependency_knowledge_point_id int8 NOT NULL, -- 依赖的知识点 ID
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_knowledge_point_dependency_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_knowledge_point_dependency.kpd_knowledge_point_id IS '知识点 ID';
COMMENT ON COLUMN public.t_knowledge_point_dependency.kpd_dependency_knowledge_point_id IS '依赖的知识点 ID';
COMMENT ON COLUMN public.t_knowledge_point_dependency.created_by IS '创建人';
COMMENT ON COLUMN public.t_knowledge_point_dependency.created_at IS '创建时间';
COMMENT ON COLUMN public.t_knowledge_point_dependency.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_knowledge_point_dependency.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_knowledge_point_dependency.id IS '主键 ID';

-- public.t_knowledge_point_document definition

CREATE TABLE IF NOT EXISTS public.t_knowledge_point_document (
	kpd_knowledge_point_id int8 NOT NULL, -- 知识点 ID
	kpd_document_key varchar NOT NULL, -- 文档 Key
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_knowledge_point_document_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_knowledge_point_document.kpd_knowledge_point_id IS '知识点 ID';
COMMENT ON COLUMN public.t_knowledge_point_document.kpd_document_key IS '文档 Key';
COMMENT ON COLUMN public.t_knowledge_point_document.created_by IS '创建人';
COMMENT ON COLUMN public.t_knowledge_point_document.created_at IS '创建时间';
COMMENT ON COLUMN public.t_knowledge_point_document.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_knowledge_point_document.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_knowledge_point_document.id IS '主键 ID';

-- public.oauth2_client_registration definition

CREATE TABLE IF NOT EXISTS public.oauth2_client_registration (
	registration_id varchar NULL,
	client_id varchar NULL,
	client_secret varchar NULL,
	authorization_grant_type text NULL,
	client_name varchar NULL,
	redirect_uri varchar NULL,
	scopes text NULL,
	client_authentication_method text NULL,
	authorization_uri varchar NULL,
	token_uri varchar NULL,
	user_info_uri varchar NULL, -- 用户信息 URI
	user_info_authentication_method text NULL, -- 用户信息鉴权方法
	user_name_attribute_name varchar NULL,
	jwk_set_uri varchar NULL,
	issuer_uri varchar NULL,
	configuration_metadata text NULL,
	from_issuer_location boolean NULL,
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT oauth2_client_registration_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.oauth2_client_registration.user_info_uri IS '用户信息 URI';
COMMENT ON COLUMN public.oauth2_client_registration.user_info_authentication_method IS '用户信息鉴权方法';
COMMENT ON COLUMN public.oauth2_client_registration.created_by IS '创建人';
COMMENT ON COLUMN public.oauth2_client_registration.created_at IS '创建时间';
COMMENT ON COLUMN public.oauth2_client_registration.updated_by IS '最后更新人';
COMMENT ON COLUMN public.oauth2_client_registration.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.oauth2_client_registration.id IS '主键 ID';

-- public.t_document definition

CREATE TABLE IF NOT EXISTS public.t_document (
	d_system_user_id varchar NOT NULL, -- 所有者 openId
	d_parent_id int8 NULL, -- 父文档 ID
	d_type text NOT NULL, -- 文档类型
	d_external_id varchar NULL, -- 外部 ID
	d_name varchar NULL, -- 文档名称
	d_order int8 DEFAULT 0 NOT NULL, -- 顺序
	created_by varchar NULL, -- 创建人
	created_at timestamp DEFAULT now() NOT NULL, -- 创建时间
	updated_by varchar NULL, -- 最后更新人
	updated_at timestamp DEFAULT now() NOT NULL, -- 最后更新时间
	id bigserial NOT NULL, -- 主键 ID
	CONSTRAINT t_document_pk PRIMARY KEY (id)
);

COMMENT ON COLUMN public.t_document.d_system_user_id IS '所有者 openId';
COMMENT ON COLUMN public.t_document.d_parent_id IS '父文档 ID';
COMMENT ON COLUMN public.t_document.d_type IS '文档类型';
COMMENT ON COLUMN public.t_document.d_external_id IS '外部 ID';
COMMENT ON COLUMN public.t_document.d_name IS '文档名称';
COMMENT ON COLUMN public.t_document.d_order IS '顺序';
COMMENT ON COLUMN public.t_document.created_by IS '创建人';
COMMENT ON COLUMN public.t_document.created_at IS '创建时间';
COMMENT ON COLUMN public.t_document.updated_by IS '最后更新人';
COMMENT ON COLUMN public.t_document.updated_at IS '最后更新时间';
COMMENT ON COLUMN public.t_document.id IS '主键 ID';

